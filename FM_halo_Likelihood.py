# A cython-enabled version of the VIRBIUS-BORG Likelihood, for use with a ROCKSTAR halo catalogue data file
# This likelihood runs the forward model using LPT
# The input data for this likelihood are Rockstar halos, with outputs in the order outputted by default (and saved in 'out.npy' file)
# The likelihood here is broadened by an additional term (denoted sigma_theory) to account for the non-linearity in the halo finding process not accounted for in the data model


import borg
import numpy as np
import cython
from get_linear_v_field import get_linear_v_field
from v_field_interpolation import interpolate_linear_v_field, interpolated_field_projection
from adjoint_gradient import tangent_adjoint_projection as tangent_adjoint_1, tangent_adjoint_interpolation as tangent_adjoint_2, tangent_adjoint_get_linear_v_field as tangent_adjoint_3

cons = borg.console()
myprint = lambda x: cons.print_std(x) if type(x) == str else cons.print_std(repr(x))

class VIRBIUS_BORG_Likelihood(borg.likelihood.BaseLikelihood):
    def __init__(self, fwd, N, L):
        myprint(f" Init {N}, {L}")
        super().__init__(fwd, N, L)


    def initializeLikelihood(self, state):
        myprint("Init likelihood")
        fwd = self.getForwardModel()
        M = 10000
        state.newArray1d('data_x',M, True)
        state.newArray1d("data_y", M, True)
        state.newArray1d("data_z", M, True)
        state.newArray1d("tracer_velocities", M, True)
        state.newArray1d('cpar',2,True)
        self.data = [state['data_x'],state['data_y'],state['data_z'],state['tracer_velocities'],state['cpar']]
        state.newArray1d("interpolated_velocities",  self.data[0].shape[0], True)
        state.newArray3d("final_density_field", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_0", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_1", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_2", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
      

    def updateMetaParameters(self, state):
        cpar = state['cosmology']
        myprint(f"Cosmology is {cpar}")
        self.getForwardModel().setCosmoParams(cpar)

    def generateMockData(self, s_hat, state):
        # Extract the BORG box parameters
        fwd = self.getForwardModel()
        N = fwd.getBoxModel().N[0]
        L = fwd.getOutputBoxModel().L[0]
        corner0 = fwd.getOutputBoxModel().xmin[0]
        corner1 = fwd.getOutputBoxModel().xmin[1]
        corner2 = fwd.getOutputBoxModel().xmin[2]
       
        # Load halo data 
        # ASSUMES DEFAULT ROCKSTAR LAYOUT OF HALO QUANTITIES
        halo_file = 'out.npy'
        hf = np.load(halo_file)
        num_halos = np.shape(hf)[0]
 
        # Draw M halos from the halo file
        M = 10000
        ids = np.arange(num_halos)
        data_x = np.zeros((num_halos))
        data_y = np.zeros((num_halos))
        data_z = np.zeros((num_halos))
        tv = np.zeros((num_halos))
        for i in range(num_halos):
            data_x[i] = hf[i][8]-125
            data_y[i] = hf[i][9]-125
            data_z[i] = hf[i][10]-125
            vx = hf[i][11]
            vy = hf[i][12]
            vz = hf[i][13]
            r = np.sqrt(data_x[i]**2 + data_y[i]**2 + data_z[i]**2)
            tv[i] = vx * data_x[i]/r + vy * data_y[i]/r + vz * data_z[i]/r
        
        # Save the coordinates 
        state['data_x'][:] = data_x
        state['data_y'][:] = data_y
        state['data_z'][:] = data_z
        #state['tracer_velocities'][:] = tv

        # Run the necessary parts of the chain
        output_density = np.zeros((N,N,N))
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)
        cpar = state['cosmology']
        cosmo_params = np.zeros((2))
        cosmo_params[0] = cpar.h
        cosmo_params[1] = cpar.omega_m
        state['cpar'][:] = cosmo_params
        # Get the linear velocity field from the density field
        output_velocity = np.zeros((3,N,N,N))     
        output_velocity = get_linear_v_field(output_density,N,L,cpar.h*100,cpar.omega_m**(5/9))
        tracer_number =  state['data_x'].shape[0]
        
        # Interpolate the linear velocity field to the tracer positions and get the line of sight component
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2 = interpolate_linear_v_field(output_velocity,self.data[0],self.data[1], self.data[2], N, L, corner0,corner1,corner2)
        interpolated_velocity = interpolated_field_projection(self.data[0],self.data[1], self.data[2],interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2)

        # Produce log likelihood
        # First add noise to tracers
        sigma_noise = 150
        state['tracer_velocities'][:] = tv + np.random.normal(0,sigma_noise,size=num_halos)
        # Choose theory uncertainty value (to account for model misspecification)
        sigma_theory = 300
        # Add errors in quadrature
        sigma_like = np.sqrt(sigma_noise**2 + sigma_theory**2)
        like = ((state['tracer_velocities'][:] - interpolated_velocity)**2/(2*sigma_like**2)).sum()
        myprint(
            f"Initial log_likelihood: {like}, var(s_hat) = {np.var(s_hat)}")


    def logLikelihoodComplex(self, s_hat, gradientIsNext):
    #def logLikelihoodComplex(self, s_hat):
        fwd = self.getForwardModel()
        N = fwd.getBoxModel().N[0]
        L = fwd.getOutputBoxModel().L[0]
        corner0 = fwd.getOutputBoxModel().xmin[0]
        corner1 = fwd.getOutputBoxModel().xmin[1]
        corner2 = fwd.getOutputBoxModel().xmin[2]
        cpar = self.data[4]
        
        # Run the necessary parts of the chain
        output_density = np.zeros((N,N,N))
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)

        # Get the linear velocity field from the density field
        output_velocity = np.zeros((3,N,N,N))     
        output_velocity = get_linear_v_field(output_density,N,L,cpar[0]*100,cpar[1]**(5/9))
        tracer_number =  self.data[0].shape[0]
        
        # Interpolate the linear velocity field to the tracer positions and get the line of sight component
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2 = interpolate_linear_v_field(output_velocity,self.data[0],self.data[1], self.data[2], N, L, corner0,corner1,corner2)
        interpolated_velocity = interpolated_field_projection(self.data[0],self.data[1], self.data[2],interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2)
        
        # Calculate the likelihood
        sigma_noise = 150
        sigma_theory = 300
        sigma_like = np.sqrt(sigma_noise**2 + sigma_theory**2)
        L = ((self.data[3] - interpolated_velocity)**2/(2*sigma_like**2)).sum()

        myprint(f"var(s_hat): {np.var(s_hat)}, Call to logLike: {L}")
        return L


    def gradientLikelihoodComplex(self, s_hat):
        fwd = self.getForwardModel()
        N = fwd.getBoxModel().N[0]
        L = fwd.getOutputBoxModel().L[0]
        corner0 = fwd.getOutputBoxModel().xmin[0]
        corner1 = fwd.getOutputBoxModel().xmin[1]
        corner2 = fwd.getOutputBoxModel().xmin[2]
        cpar = self.data[4]
        
        # Run the necessary parts of the chain
        output_density = np.zeros((N,N,N))
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)

        # Get the linear velocity field from the density field
        output_velocity = np.zeros((3,N,N,N))     
        output_velocity = get_linear_v_field(output_density,N,L,cpar[0]*100,cpar[1]**(5/9))
        tracer_number =  self.data[0].shape[0]
        
        # Interpolate the linear velocity field to the tracer positions and get the line of sight component
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2 = interpolate_linear_v_field(output_velocity,self.data[0],self.data[1], self.data[2], N, L, corner0,corner1,corner2)
        interpolated_velocity = interpolated_field_projection(self.data[0],self.data[1], self.data[2],interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2)
        
        sigma_noise = 150
        sigma_theory = 300
        sigma_like = np.sqrt(sigma_noise**2 + sigma_theory**2)
        v_tracer_tilde = -(self.data[3] - interpolated_velocity)/sigma_like**2
        v_tracer_tilde_0,v_tracer_tilde_1,v_tracer_tilde_2 = tangent_adjoint_1(self.data[0], self.data[1], self.data[2],v_tracer_tilde)
        v_grid_tilde_0,v_grid_tilde_1,v_grid_tilde_2 = tangent_adjoint_2(self.data[0], self.data[1], self.data[2],N,L,corner0,corner1,corner2,v_tracer_tilde_0,v_tracer_tilde_1,v_tracer_tilde_2)
        delta_F_tilde = tangent_adjoint_3(N, L, cpar[0]*100, cpar[1]**(5/9), v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2)
        
        # The output of the above is an array of size (N,N,N) which we denote as delta_F_tilde. This is what we feed to pyborg machinery.
        fwd.adjointModel_v2(delta_F_tilde)
        mygrad_hat = np.zeros(s_hat.shape, dtype=np.complex128)
        fwd.getAdjointModel(mygrad_hat)
        
        return -mygrad_hat


model = None  
  
@borg.registerGravityBuilder
def build_gravity_model(state, box):
    global model
    chain = borg.forward.ChainForwardModel(box)
    chain.addModel(borg.forward.models.HermiticEnforcer(box))
    chain.addModel(borg.forward.models.Primordial(box, 0.001))
    chain.addModel(borg.forward.models.EisensteinHu(box))
    lpt = borg.forward.models.BorgLpt(box=box,box_out=box,ai=0.001,af=1.0,supersampling=2)
    chain.addModel(lpt)
    model = chain
    return chain


@borg.registerLikelihoodBuilder
def build_likelihood(state, info):
    boxm = model.getBoxModel()
    return VIRBIUS_BORG_Likelihood(model, boxm.N, boxm.L)

@borg.registerSamplerBuilder
def build_sampler(state, info):
    #sampler = borg.samplers.HMCDensitySampler(VIRBIUS_BORG_Likelihood)
    return []  
