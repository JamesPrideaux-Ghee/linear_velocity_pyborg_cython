import numpy as np
cimport cython
cimport numpy as np
from libc.math cimport floor
ctypedef np.int_t DTYPE_t
ctypedef np.double_t DTYPE_f
ctypedef np.cdouble_t DTYPE_c


@cython.cdivision(True)
@cython.boundscheck(False)
cpdef tangent_adjoint_projection(np.ndarray[DTYPE_f, ndim=1] data_x, np.ndarray[DTYPE_f, ndim=1] data_y, np.ndarray[DTYPE_f, ndim=1] data_z, np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde):
    """Function to calculate the step of the tangent adjoint calculation going from the line of sight velocity field at the tracers to the velocity field at the tracers. 
    arguments:
    - data_x: a vector containing the x coordinates of the tracers.
    - data_y: a vector containing the y coordinates of the tracers.
    - data_z: a vector containing the z coordinates of the tracers.
    - v_tracer_tilde: a vector containing the first step of the tangent adjoint calculation
    outputs:
    - v_tracer_tilde_0: an (N,N,N) array with the x component of the v_tracer_tilde field 
    - v_tracer_tilde_1: an (N,N,N) array with the y component of the v_tracer_tilde field
    - v_tracer_tilde_2: an (N,N,N) array with the z component of the v_tracer_tilde field
    """
    cdef Py_ssize_t l
    cdef int tracer_number
    tracer_number = len(data_x)
    cdef long double r
    cdef np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde_0 = np.zeros([tracer_number], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde_1 = np.zeros([tracer_number], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde_2 = np.zeros([tracer_number], dtype = np.double)
    cdef double [:] v_tracer_tilde_0_view = v_tracer_tilde_0
    cdef double [:] v_tracer_tilde_1_view = v_tracer_tilde_1 
    cdef double [:] v_tracer_tilde_2_view = v_tracer_tilde_2 
    
    for l in range(tracer_number):
        r = (data_x[l]**2+data_y[l]**2 + data_z[l]**2)**(0.5)
        v_tracer_tilde_0_view[l] = v_tracer_tilde[l] * data_x[l]/r
        v_tracer_tilde_1_view[l] = v_tracer_tilde[l] * data_y[l]/r
        v_tracer_tilde_2_view[l] = v_tracer_tilde[l] * data_z[l]/r
    return v_tracer_tilde_0, v_tracer_tilde_1, v_tracer_tilde_2


@cython.cdivision(True)
@cython.boundscheck(False)
cpdef tangent_adjoint_interpolation(np.ndarray[DTYPE_f, ndim=1] data_x, np.ndarray[DTYPE_f, ndim=1] data_y, np.ndarray[DTYPE_f, ndim=1] data_z, int N, float L, float corner0, float corner1, float corner2, np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde_0, np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde_1, np.ndarray[DTYPE_f, ndim=1] v_tracer_tilde_2):
    """ This function deposits interpolated velocity field components from the the tracer positions to the grid
    arguments:
    - data_x: numpy array of length (Ndata) containing the comoving x coordinates of the tracers
    - data_y: numpy array of length (Ndata) containing the comoving y coordinates of the tracers
    - data_z: numpy array of length (Ndata) containing the comoving z coordinates of the tracers
    - N: int, number of voxels per side in the BORG cube
    - L: float, side length of the BORG cube (in Mpc/h)
    - corner0: lowest x value in the BORG cube
    - corner1: lowest y value in the BORG cube
    - corner2: lowest z value in the BORG cube
    - v_tracer_tilde_0: numpy array of length (Ndata); first output of function tangent_adjoint_projection
    - v_tracer_tilde_1: numpy array of length (Ndata); second output of function tangent_adjoint_projection
    - v_tracer_tilde_2: numpy array of length (Ndata); third output of function tangent_adjoint_projection
    outputs:
    - v_grid_tilde_0: numoy array of shape (N,N,N) containing object in space of the x component of the velocity field on the grid
    - v_grid_tilde_1: numpy array of shape (N,N,N) containing object in space of the y component of the velocity field on the grid 
    - v_grid_tilde_2: numpy array of shape (N,N,N) containing object in space of the z component of the velocity field on the grid
    """
    # Define variables
    cdef int tracer_number
    cdef double delta = L/N
    tracer_number = len(data_x)
    cdef double rx = 0
    cdef double ry = 0
    cdef double rz = 0
    cdef double qx = 0
    cdef double qy = 0
    cdef double qz = 0
    cdef Py_ssize_t l
    cdef Py_ssize_t ix, iy, iz, jx, jy, jz
    cdef double dx,dy,dz, x, y, z, hx, hy, hz
    #cdef double [:] grid_axis_x = np.zeros([N], dtype = np.double)
    #cdef double [:] grid_axis_y = np.zeros([N], dtype = np.double)
    #cdef double [:] grid_axis_z = np.zeros([N], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=3] v_grid_tilde_0 = np.zeros([N,N,N], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=3] v_grid_tilde_1 = np.zeros([N,N,N], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=3] v_grid_tilde_2 = np.zeros([N,N,N], dtype = np.double)
    cdef double [:,:,:] v_grid_tilde_0_view = v_grid_tilde_0
    cdef double [:,:,:] v_grid_tilde_1_view = v_grid_tilde_1
    cdef double [:,:,:] v_grid_tilde_2_view = v_grid_tilde_2
        
    for l in range(tracer_number):  
        # Shift data by the box corners
        dx = data_x[l] - corner0
        dy = data_y[l] - corner1
        dz = data_z[l] - corner2
        # Apply periodic boundary conditions to handle cases where tracers are outside of box
        if dx >= L:
            dx -= L
        if dx < 0:
            dx += L
        if dy >= L:
            dy -= L
        if dy < 0:
            dy += L
        if dz >= L:
            dz -= L
        if dz < 0:
            dz += L
        # Rescale
        x = dx/delta
        y = dy/delta
        z = dz/delta
        # We find the indices of the voxel vertex with the lowest x, y, and z values
        hx = floor(x)
        hy = floor(y)
        hz = floor(z)
        # We get the int type version.
        ix = int(hx)
        iy = int(hy)
        iz = int(hz)
        # Get adjacent vertix indices
        jx = ix + 1 
        jy = iy + 1
        jz = iz + 1
        # Handle overflow cases
        if jx >= N:
            jx -= N
        if jy >= N: 
            jy -= N
        if jz >= N:
            jz -=N
        # Get distances to vertex
        rx = x - hx 
        ry = y - hy
        rz = z - hz
        # Get complementary distances
        qx = 1 - rx
        qy = 1 - ry
        qz = 1 - rz
        # Deposit components from the tracer to the voxel vertices
        # x component of velocity 
        v_grid_tilde_0_view[ix,iy,iz] += qx * qy * qz * v_tracer_tilde_0[l]
        v_grid_tilde_0_view[jx,iy,iz] += rx * qy * qz * v_tracer_tilde_0[l] 
        v_grid_tilde_0_view[ix,jy,iz] += qx * ry * qz * v_tracer_tilde_0[l]
        v_grid_tilde_0_view[ix,iy,jz] += qx * qy * rz * v_tracer_tilde_0[l]
        v_grid_tilde_0_view[jx,jy,iz] += rx * ry * qz * v_tracer_tilde_0[l]
        v_grid_tilde_0_view[jx,iy,jz] += rx * qy * rz * v_tracer_tilde_0[l]
        v_grid_tilde_0_view[ix,jy,jz] += qx * ry * rz * v_tracer_tilde_0[l]
        v_grid_tilde_0_view[jx,jy,jz] += rx * ry * rz * v_tracer_tilde_0[l]
        # y component of velocity 
        v_grid_tilde_1_view[ix,iy,iz] += qx * qy * qz * v_tracer_tilde_1[l]
        v_grid_tilde_1_view[jx,iy,iz] += rx * qy * qz * v_tracer_tilde_1[l] 
        v_grid_tilde_1_view[ix,jy,iz] += qx * ry * qz * v_tracer_tilde_1[l]
        v_grid_tilde_1_view[ix,iy,jz] += qx * qy * rz * v_tracer_tilde_1[l]
        v_grid_tilde_1_view[jx,jy,iz] += rx * ry * qz * v_tracer_tilde_1[l]
        v_grid_tilde_1_view[jx,iy,jz] += rx * qy * rz * v_tracer_tilde_1[l]
        v_grid_tilde_1_view[ix,jy,jz] += qx * ry * rz * v_tracer_tilde_1[l]
        v_grid_tilde_1_view[jx,jy,jz] += rx * ry * rz * v_tracer_tilde_1[l]
        # z component of velocity 
        v_grid_tilde_2_view[ix,iy,iz] += qx * qy * qz * v_tracer_tilde_2[l]
        v_grid_tilde_2_view[jx,iy,iz] += rx * qy * qz * v_tracer_tilde_2[l] 
        v_grid_tilde_2_view[ix,jy,iz] += qx * ry * qz * v_tracer_tilde_2[l]
        v_grid_tilde_2_view[ix,iy,jz] += qx * qy * rz * v_tracer_tilde_2[l]
        v_grid_tilde_2_view[jx,jy,iz] += rx * ry * qz * v_tracer_tilde_2[l]
        v_grid_tilde_2_view[jx,iy,jz] += rx * qy * rz * v_tracer_tilde_2[l]
        v_grid_tilde_2_view[ix,jy,jz] += qx * ry * rz * v_tracer_tilde_2[l]
        v_grid_tilde_2_view[jx,jy,jz] += rx * ry * rz * v_tracer_tilde_2[l]
    return v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2
    
#### tangent_adjoint_get_linear_v_field

def tangent_adjoint_get_linear_v_field(DTYPE_t N, double L, float H, float f, np.ndarray[DTYPE_f, ndim=3] v_grid_tilde_0, np.ndarray[DTYPE_f, ndim=3] v_grid_tilde_1, np.ndarray[DTYPE_f, ndim=3] v_grid_tilde_2):# double [:,:,:] v_grid_tilde_0, double [:,:,:] v_grid_tilde_1, double [:,:,:] v_grid_tilde_2):
    """Function to calculate the step of the tangent adjoint calculation going from the velocity field on the grid to the final density field. 
    arguments:
    - N: the number of voxels per side of the BORG grid.
    - D_0: an (N,N,N) array containing the Finite Difference Approximation Fourier space kernel.
    - D_1: an (N,N,N) array containing the Finite Difference Approximation Fourier space kernel.
    - D_2: an (N,N,N) array containing the Finite Difference Approximation Fourier space kernel.
    - v_grid_tilde_0: an (N,N,N) array containing the output of the previous steps of the tangent adjoint calculation.
    - v_grid_tilde_0: an (N,N,N) array containing the output of the previous steps of the tangent adjoint calculation.
    - v_grid_tilde_0: an (N,N,N) array containing the output of the previous steps of the tangent adjoint calculation.
    outputs:
    - an (N,N,N) array to be fed to adjointModel_v2
    """
    cdef np.ndarray[DTYPE_f, ndim=3] output = np.zeros([N,N,N], dtype = np.double)
    cdef np.ndarray[DTYPE_c, ndim=3] G_0_tilde = np.zeros([N,N,N//2+1], dtype = np.cdouble)
    cdef np.ndarray[DTYPE_c, ndim=3] G_1_tilde = np.zeros([N,N,N//2+1], dtype = np.cdouble)
    cdef np.ndarray[DTYPE_c, ndim=3] G_2_tilde = np.zeros([N,N,N//2+1], dtype = np.cdouble)
    cdef np.ndarray[np.double_t, ndim=1] ik = np.zeros([N], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_0 = np.zeros([N,N,N//2+1], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_1 = np.zeros([N,N,N//2+1], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_2 = np.zeros([N,N,N//2+1], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_sq = np.zeros([N,N,N//2+1], dtype = np.double)
    
    # We get the Fourier modes
    ik = np.fft.fftfreq(N,d=L/N) * 2 * np.pi
    # Meshgrid outputs are in a weird order due to its indexing
    k_0,k_1,k_2 = np.meshgrid(ik,ik,ik[0:N//2+1],indexing='ij')
    k_sq = k_0**2 + k_1**2 + k_2**2
    G_0_tilde = -1j * 100 * f * k_0 / (k_sq)
    G_1_tilde = -1j * 100 * f * k_1 / (k_sq)
    G_2_tilde = -1j * 100 * f * k_2 / (k_sq)
    #print('Kernel data type')
    #print(G_0_tilde.dtype)
    # We enforce Nyquist planes - remove Fourier mode ambiguity
    G_0_tilde[N//2,:,:] = 0
    G_1_tilde[:,N//2,:] = 0
    G_2_tilde[:,:,N//2] = 0
    # We handle the case where there is k mode division by zero
    G_0_tilde[k_sq==0] = 0
    G_1_tilde[k_sq==0] = 0
    G_2_tilde[k_sq==0] = 0
    output = -np.fft.irfftn( G_0_tilde * np.fft.rfftn(v_grid_tilde_0))
    output += -np.fft.irfftn( G_1_tilde * np.fft.rfftn(v_grid_tilde_1))
    output += -np.fft.irfftn( G_2_tilde * np.fft.rfftn(v_grid_tilde_2)) 
    #output = np.fft.irfftn( np.multiply(G_0_tilde[:,:,0:N//2+1], np.fft.rfftn(v_grid_tilde_0))) + np.fft.irfftn( np.multiply(G_1_tilde[:,:,0:N//2+1], np.fft.rfftn(v_grid_tilde_1))) + np.fft.irfftn( np.multiply(G_2_tilde[:,:,0:N//2+1], np.fft.rfftn(v_grid_tilde_2)))
    return output
###
