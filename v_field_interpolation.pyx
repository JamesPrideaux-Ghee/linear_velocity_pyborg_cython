import numpy as np
cimport cython
cimport numpy as np
ctypedef np.int_t DTYPE_t
ctypedef np.double_t DTYPE_f
from libc.math cimport floor


@cython.cdivision(True)
@cython.boundscheck(False)    
cpdef interpolate_linear_v_field(np.ndarray[DTYPE_f, ndim=4] v_field, np.ndarray[DTYPE_f, ndim=1] data_x, np.ndarray[DTYPE_f, ndim=1] data_y, np.ndarray[DTYPE_f, ndim=1] data_z, int N, float L, float corner0, float corner1, float corner2):
    """ This function deposits linear velocity field components from the grid to the tracer positions
    arguments:
    - v_field: numpy array of shape (3,N,N,N) containing the velocity field on the grid
    - data_x: numpy array of length (Ndata) containing the comoving x coordinates of the tracers
    - data_y: numpy array of length (Ndata) containing the comoving y coordinates of the tracers
    - data_z: numpy array of length (Ndata) containing the comoving z coordinates of the tracers
    - N: int, number of voxels per side in the BORG cube
    - L: float, side length of the BORG cube (in Mpc/h)
    - corner0: float, lowest x value in the BORG cube
    - corner1: float, lowest y value in the BORG cube
    - corner2: float, lowest z value in the BORG cube
    outputs:
    - interpolated_velocity_0: numpy array of length (Ndata) containing the x component of the velocity field interpolated to tracer positions
    - interpolated_velocity_1: numpy array of length (Ndata) containing the y component of the velocity field interpolated to tracer positions
    - interpolated_velocity_2: numpy array of length (Ndata) containing the z component of the velocity field interpolated to tracer positions
    """
    # Define variables
    cdef int tracer_number # the number of tracers
    tracer_number = len(data_x)
    cdef double delta = L/N # physical spacing between voxel centres
    cdef Py_ssize_t l # loop index
    cdef Py_ssize_t ix, iy, iz, jx, jy, jz # indices of voxels to interpolate from
    cdef double dx,dy,dz,x,y,z,hx,hy,hz
    cdef double rx = 0
    cdef double ry = 0
    cdef double rz = 0
    cdef double qx = 0
    cdef double qy = 0
    cdef double qz = 0
    cdef np.ndarray[DTYPE_f, ndim=1] interpolated_v_field_0 = np.zeros([tracer_number], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=1] interpolated_v_field_1 = np.zeros([tracer_number], dtype = np.double)
    cdef np.ndarray[DTYPE_f, ndim=1] interpolated_v_field_2 = np.zeros([tracer_number], dtype = np.double)
    cdef double [:] interpolated_v_field_0_view =interpolated_v_field_0
    cdef double [:] interpolated_v_field_1_view =interpolated_v_field_1
    cdef double [:] interpolated_v_field_2_view =interpolated_v_field_2
        
    for l in range(tracer_number):  
        # We shift the data based on the data cube corners.
        dx = data_x[l] - corner0
        dy = data_y[l] - corner1
        dz = data_z[l] - corner2
        # We apply the periodic boundary conditions (which handles the cases where the tracer may lie outside the box)
        if dx >= L:
            dx -= L
        if dx < 0:
            dx += L
        if dy >= L:
            dy -= L
        if dy < 0:
            dy += L
        if dz >= L:
            dz -= L
        if dz < 0: 
            dz += L
        # We rescale
        x = dx/delta
        y = dy/delta
        z = dz/delta
        # We find the indices of the vertex with the lowest x, y, and z values of the voxel in which the tracer lies.
        # Note: to use these as array indices, we must apply the int() function
        hx = floor(x)
        hy = floor(y)
        hz = floor(z)
        # We find the objects as actual indices.
        ix = int(hx)
        iy = int(hy)
        iz = int(hz)
        # We find the indices of the other vertices. 
        jx = ix + 1
        jy = iy + 1
        jz = iz + 1
        # We handle the cases where these indices may lie outside the box
        if jx >= N:
            jx -= N
        if jy >= N:
            jy -= N
        if jz >= N:
            jz -= N
        # We get the distances to the lowest index vertex.
        rx = x - hx 
        ry = y - hy
        rz = z - hz
        # We get the complementary distances
        qx = 1 - rx
        qy = 1 - ry
        qz = 1 - rz

        # Sum up contributions from each velocity component from the 8 voxels
        # x component of velocity 
        interpolated_v_field_0_view[l] += qx * qy * qz * v_field[0,ix,iy,iz] 
        interpolated_v_field_0_view[l] += rx * qy * qz * v_field[0,jx,iy,iz] 
        interpolated_v_field_0_view[l] += qx * ry * qz * v_field[0,ix,jy,iz]
        interpolated_v_field_0_view[l] += qx * qy * rz * v_field[0,ix,iy,jz]
        interpolated_v_field_0_view[l] += rx * ry * qz * v_field[0,jx,jy,iz]
        interpolated_v_field_0_view[l] += rx * qy * rz * v_field[0,jx,iy,jz]
        interpolated_v_field_0_view[l] += qx * ry * rz * v_field[0,ix,jy,jz]
        interpolated_v_field_0_view[l] += rx * ry * rz * v_field[0,jx,jy,jz]
        # y component of velocity 
        interpolated_v_field_1_view[l] += qx * qy * qz * v_field[1,ix,iy,iz] 
        interpolated_v_field_1_view[l] += rx * qy * qz * v_field[1,jx,iy,iz] 
        interpolated_v_field_1_view[l] += qx * ry * qz * v_field[1,ix,jy,iz]
        interpolated_v_field_1_view[l] += qx * qy * rz * v_field[1,ix,iy,jz]
        interpolated_v_field_1_view[l] += rx * ry * qz * v_field[1,jx,jy,iz]
        interpolated_v_field_1_view[l] += rx * qy * rz * v_field[1,jx,iy,jz]
        interpolated_v_field_1_view[l] += qx * ry * rz * v_field[1,ix,jy,jz]
        interpolated_v_field_1_view[l] += rx * ry * rz * v_field[1,jx,jy,jz]
        # z component of velocity 
        interpolated_v_field_2_view[l] += qx * qy * qz * v_field[2,ix,iy,iz] 
        interpolated_v_field_2_view[l] += rx * qy * qz * v_field[2,jx,iy,iz] 
        interpolated_v_field_2_view[l] += qx * ry * qz * v_field[2,ix,jy,iz]
        interpolated_v_field_2_view[l] += qx * qy * rz * v_field[2,ix,iy,jz]
        interpolated_v_field_2_view[l] += rx * ry * qz * v_field[2,jx,jy,iz]
        interpolated_v_field_2_view[l] += rx * qy * rz * v_field[2,jx,iy,jz]
        interpolated_v_field_2_view[l] += qx * ry * rz * v_field[2,ix,jy,jz]
        interpolated_v_field_2_view[l] += rx * ry * rz * v_field[2,jx,jy,jz]
        
    return interpolated_v_field_0, interpolated_v_field_1, interpolated_v_field_2


@cython.cdivision(True)
@cython.boundscheck(False)    
cpdef interpolated_field_projection(np.ndarray[DTYPE_f, ndim=1] data_x,np.ndarray[DTYPE_f, ndim=1] data_y,np.ndarray[DTYPE_f, ndim=1] data_z,np.ndarray[DTYPE_f, ndim=1] interpolated_v_field_0,np.ndarray[DTYPE_f, ndim=1] interpolated_v_field_1,np.ndarray[DTYPE_f, ndim=1] interpolated_v_field_2):
    """ This functions gets the line of sight velocity field at the tracers from the velocity field at the tracers
    arguments:
    - data_x: numpy array of length (Ndata) containing the comoving x coordinates of tracers
    - data_y: numpy array of length (Ndata) containing the comoving y coordinates of tracers
    - data_z: numpy array of length (Ndata) containing the comoving z coordinates of tracers
    - interpolated_v_field_0: numpy array of length (Ndata) containing the x component of the velocity field interpolated to tracer positions
    - interpolated_v_field_1: numpy array of length (Ndata) containing the y component of the velocity field interpolated to tracer positions
    - interpolated_v_field_2: numpy array of length (Ndata) containing the z component of the velocity field interpolated to tracer positions
    outputs:
    - interpolated_v_field: numpy array of length (Ndata) containing the line-of-sight component of the velocity field interpolated to tracer positions
    """
    cdef int tracer_number 
    tracer_number = len(data_x)
    cdef double r
    cdef Py_ssize_t l
    cdef np.ndarray[DTYPE_f, ndim=1] interpolated_v_field = np.zeros([tracer_number], dtype = np.double)
    cdef double [:] interpolated_v_field_view =interpolated_v_field
    
    for l in range(tracer_number):
        r = (data_x[l]**2+data_y[l]**2 + data_z[l]**2)**(0.5)
        interpolated_v_field_view[l] = interpolated_v_field_0[l] * data_x[l]/r + interpolated_v_field_1[l] * data_y[l]/r + interpolated_v_field_2[l] * data_z[l]/r
    
    return interpolated_v_field
