import numpy as np
cimport cython
cimport numpy as np

ctypedef np.int_t DTYPE_t
ctypedef np.double_t DTYPE_f


def get_linear_v_field(double[:,:,:] density, int N, float L, float H, float f):
    """Function to calculate the linear (first order Eulerian peturbation theory) velocity field on the grid
    given by div . v = -H * density
    arguments:
    - density: the density contrast field on the grid.
    - box: the BORG box produced by borg.forward.BoxModel().
    - H: the value of the Hubble constant at the redshift of interest (H, not h). 
    - f: the linear growth factor (=Om ^(5/9) for a flat LambdaCDM model)
    outputs:
    - liner_v: the first order Eulerian perturbation theory velocity field on the grid.
    """
    # Initialise variables
    cdef np.ndarray[np.double_t, ndim=4] linear_v = np.zeros([3,N,N,N], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=1] ik = np.zeros([N], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_0 = np.zeros([N,N,N//2 + 1], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_1 = np.zeros([N,N,N//2 + 1], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_2 = np.zeros([N,N,N//2 + 1], dtype = np.double)
    cdef np.ndarray[np.double_t, ndim=3] k_sq = np.zeros([N,N,N//2 + 1], dtype = np.double)
    cdef np.ndarray[np.cdouble_t, ndim=3] G_0 = np.zeros([N,N,N//2 + 1], dtype = np.cdouble)
    cdef np.ndarray[np.cdouble_t, ndim=3] G_1 = np.zeros([N,N,N//2 + 1], dtype = np.cdouble)
    cdef np.ndarray[np.cdouble_t, ndim=3] G_2 = np.zeros([N,N,N//2 + 1], dtype = np.cdouble)
    cdef np.ndarray[np.cdouble_t, ndim=3] density_tilde = np.zeros([N,N,N//2 + 1], dtype = np.cdouble)
    
    # We get the Fourier modes
    ik = np.fft.fftfreq(N,d=L/N) * 2 * np.pi
    # indexing='ij' needed otherwise meshgrid output are in an odd order
    k_0,k_1,k_2 = np.meshgrid(ik,ik,ik[0:N//2+1],indexing='ij')
    k_sq = k_0**2 + k_1**2 + k_2**2
    # Add the finite difference operators that give the discrete equivalent of the gradient operator Green's function.
    G_0 = -1j * 100 * f * k_0/(k_sq)
    G_1 = -1j * 100 * f * k_1/(k_sq)
    G_2 = -1j * 100 * f * k_2/(k_sq)
    # We set the planes in the kernel where the Fourier frequency components are at the Nyquist frequency to zero.
    # This solves the ambiguity in the frequency sign at these planes.
    G_0[N//2,:,:] = 0
    G_1[:,N//2,:] = 0
    G_2[:,:,N//2] = 0
    # We set the mode where k_sq = 0 to zero to handle any division by zero cases
    G_0[k_sq==0] = 0 
    G_1[k_sq==0] = 0
    G_2[k_sq==0] = 0
    # Calculate the velocity field given by div v = - H * f * density
    density_tilde = np.fft.rfftn(density)
    linear_v[0,:,:,:] = -np.fft.irfftn( G_0 * density_tilde)
    linear_v[1,:,:,:] = -np.fft.irfftn( G_1 * density_tilde)
    linear_v[2,:,:,:] = -np.fft.irfftn( G_2 * density_tilde)
    return linear_v

