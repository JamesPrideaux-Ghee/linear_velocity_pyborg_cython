from tqdm import tqdm
import borg
import numpy as np
from get_linear_v_field import get_linear_v_field
from adjoint_gradient import tangent_adjoint_2

# build the structure formation model we want
def build_gravity_model(state, box):
    global model
    chain = borg.forward.ChainForwardModel(box)
    chain.addModel(borg.forward.models.HermiticEnforcer(box))
    chain.addModel(borg.forward.models.Primordial(box,0.001))
    chain.addModel(borg.forward.models.EisensteinHu(box))
    lpt = borg.forward.models.BorgLpt(box=box,box_out=box,ai=0.001,af=1.0,supersampling=1)
    chain.addModel(lpt)
    model = chain
    return chain    


# Set up box parameters
N =  8
L = 100.
xmin = -L/2
delta = L/N
cpar = borg.cosmo.CosmologicalParameters()
state = borg.likelihood.MarkovState()
info = borg.likelihood.LikelihoodInfo()
info["GRID_LENGTH"] = np.array([-L//2, L//2, -L//2, L//2, -L//2, L//2])
info["GRID"] = np.array([N, N, N], dtype=np.uint32)
box = borg.forward.BoxModel(L, N)

# Set up an example Markov State
# We only add the necessary parts here
chain = build_gravity_model(state,box)
state.newForwardModel("BORG_model",chain)
state.newScalar("cosmology",cpar)
state.newScalar("corner0",-L/2)
state.newScalar("corner1",-L/2)
state.newScalar("corner2",-L/2)

# Generate a GRF field as the initial conditions from which we run the forward model
s_hat = np.fft.rfftn(np.random.randn(N, N, N) / N**(1.5))

# Run the forward model
output_density = np.zeros((N,N,N))
chain.forwardModel_v2(s_hat)
chain.getDensityFinal(output_density)

# Get the velocity field
output_velocity = np.zeros((3,N,N,N))
output_velocity = get_linear_v_field(output_density,N,L,cpar.h*100,cpar.omega_m**(5/9))

# Set up simple test cases for the tangent adjoint interpolation
# - voxel lower vertices occur at:
vertices = np.linspace(xmin,xmin+L-delta,N)
test_no = 1
if test_no == 1:
    # Single tracer at (1/2,1/2,1/2)*delta from lowest index vertix
    data_x = np.zeros((1))
    data_y = np.zeros((1))
    data_z = np.zeros((1))
    v_t_0 = np.zeros((1))
    v_t_1 = np.zeros((1))
    v_t_2 = np.zeros((1))
    data_x[0] = xmin+delta/2
    data_y[0] = xmin+delta/2
    data_z[0] = xmin+delta/2
    v_t_0[0] = 1
    v_t_1[0] = 2
    v_t_2[0] = 3
if test_no == 2:
    # - 1st test: put a single tracer (1/2,1/2,1/2)*delta from the lowest index vertex
    # - use a constant v field as the quantity to be interpolated
    data_x = np.zeros((1))
    data_y = np.zeros((1))
    data_z = np.zeros((1))
    data_x[0] = xmin+delta/2
    data_y[0] = xmin+delta/2
    data_z[0] = xmin+delta/2
    test_v = np.ones((3,N,N,N))
    test_v[1,:,:,:] *= 2
    test_v[2,:,:,:] *= 3
if test_no ==3:
    # - 2nd test: single tracer (1/2,1/2,1)*delta from lowest index vertex. 
    # - same constant v field at in 1st test
    data_x[0] = xmin+delta/2
    data_y[0] = xmin+delta/2
    data_z[0] = xmin+delta
if test_no == 4:
    # - 3rd test: single tracer at (1/2,1/2,1/2)*delta from lowest index vertex.
    # - non-constant v field
    data_x[0] = xmin+delta/2
    data_y[0] = xmin+delta/2
    data_z[0] = xmin+delta/2
    test_v = np.ones((3,N,N,N))
    test_v[0,:,:,1] += 1
if test_no == 5:
    # - 4th test: single tracer at (1,3/4,3/4)*delta
    # - same non-constant v as in 3 
    data_x[0] = xmin+delta
    data_y[0] = xmin+3/4 * delta
    data_z[0] = xmin+3/4 * delta
if test_no ==6:
    # - 5th testL try underflow case with tracer at (-1,3/4,3/4)*delta
    # - same non-constant v
    data_x[0] = xmin-delta
    data_y[0] = xmin+3/4 * delta
    data_z[0] = xmin+3/4 * delta
if test_no == 7:
    # - 6th test: try overflow case with tracer at (L/2 +delta,3/4delta,3/4delta)
    # - same non-constant v (should be same result as 4 and 5)
    data_x[0] = xmin+L+delta
    data_y[0] = xmin+3/4 * delta
    data_z[0] = xmin+3/4 * delta
if test_no == 8:
    # - 7th test: tracer exactly on lowest index vertex
    data_x[0] = xmin
    data_y[0] = xmin
    data_z[0] = xmin

vg_0, vg_1, vg_2 = tangent_adjoint_2(data_x,data_y,data_z,N,L,xmin,xmin,xmin,v_t_0,v_t_1,v_t_2)
np.savez('tangent_test.npz',vg_0 = vg_0, vg_1 = vg_0, vg_2 = vg_2)



# Get the analytic adjoint gradient
#analytic_gradient = like.gradientLikelihood(s_hat)
#np.savez("gradients.npz", num=num_gradient, ana=analytic_gradient)
