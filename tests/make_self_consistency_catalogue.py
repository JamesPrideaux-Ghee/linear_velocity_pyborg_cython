import borg
import numpy as np
import cython
from get_linear_v_field import get_linear_v_field
import h5py as h5

# Set up the BORG box
print('Set up box')
Ng =  32
cpar = borg.cosmo.CosmologicalParameters()    
state = borg.likelihood.MarkovState()
info = borg.likelihood.LikelihoodInfo()
info["GRID_LENGTH"] = np.array([0, 200., 0, 200., 0, 200.])
info["GRID"] = np.array([Ng, Ng, Ng], dtype=np.uint32)
box = borg.forward.BoxModel(200., Ng)

L = 200
corner0 = -L/2
corner1 = -L/2
corner2 = -L/2
delta = L/Ng
grid_axis_0 = np.linspace(corner0,corner0+L-delta,Ng)
grid_axis_1 = np.linspace(corner1,corner1+L-delta,Ng)
grid_axis_2 = np.linspace(corner2,corner2+L-delta,Ng)
grid_y, grid_x, grid_z = np.meshgrid(grid_axis_0,grid_axis_1,grid_axis_2)

# Set up the chain used for the forward model
print('Set up chain')
chain = borg.forward.ChainForwardModel(box)
chain.addModel(borg.forward.models.HermiticEnforcer(box))
chain.addModel(borg.forward.models.Primordial(box, 0.001))
chain.addModel(borg.forward.models.EisensteinHu(box))
lpt = borg.forward.models.BorgLpt(box=box,box_out=box,ai=0.001,af=1.0,supersampling=2)
chain.addModel(lpt)
cosmo_par = borg.cosmo.CosmologicalParameters()
cosmo_par.default()
cosmo_par.omega_m = 0.3
cosmo_par.omega_q = 0.7
cosmo_par.sigma8 = 0.83
cosmo_par.h = 0.7
cosmo_par.n_s = 0.96
cosmo_par.omega_b = 0.05
chain.setCosmoParams(cosmo_par)

# Define the initial conditions field. For a self-consistency check, we want the chain to be able to reproduce this field.
print('Set up s_hat')
ic = np.fft.rfftn(np.random.randn(Ng, Ng, Ng) / Ng**(1.5))

# We run the forward model.
print('Run forward model')
chain.forwardModel_v2(ic)  
final_density = np.empty(chain.getOutputBoxModel().N)
chain.getDensityFinal(final_density)  

# We extract the linear velocity field. 
print('Get linear velocity')
linear_v = get_linear_v_field(final_density,Ng,200,cosmo_par.h*100,cosmo_par.omega_m**(5/9))
v_0 = linear_v[0,:,:,:]
v_1 = linear_v[1,:,:,:]
v_2 = linear_v[2,:,:,:]

print('Get radial velocity')
los_velocity = np.zeros((Ng,Ng,Ng))
radial_distance_grid = np.sqrt(grid_x**2 + grid_y**2 + grid_z**2)
los_velocity = v_0*grid_x/radial_distance_grid + v_1*grid_y/radial_distance_grid+v_2*grid_z/radial_distance_grid
    

# We save the initial conditions, the final density, and the linear velocity field. 
print('Save')
fdir = ''
file = 'fields_for_tracers.h5'
hf = h5.File(fdir+file,'w')
hf.create_dataset('initial_conditions',data=ic)
hf.create_dataset('final_density',data=final_density)
hf.create_dataset('linear_velocity',data=linear_v)
hf.create_dataset('los_velocity',data=los_velocity)
hf.close()

   
