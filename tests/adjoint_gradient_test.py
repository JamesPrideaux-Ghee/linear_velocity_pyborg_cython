from tqdm import tqdm
import borg
import numpy as np
from Likelihood import VIRBIUS_BORG_Likelihood

# build the structure formation model we want
def build_gravity_model(state, box):
    global model
    chain = borg.forward.ChainForwardModel(box)
    chain.addModel(borg.forward.models.HermiticEnforcer(box))
    chain.addModel(borg.forward.models.Primordial(box,0.001))
    chain.addModel(borg.forward.models.EisensteinHu(box))
    lpt = borg.forward.models.BorgLpt(box=box,box_out=box,ai=0.001,af=1.0,supersampling=1)
    chain.addModel(lpt)
    model = chain
    return chain    


def build_likelihood(state, info):
    boxm = model.getBoxModel()    
    return VIRBIUS_BORG_Likelihood(model,boxm.N,boxm.L)


# Set up box parameters
N =  8
L = 100.
xmin = -L/2
cpar = borg.cosmo.CosmologicalParameters()
state = borg.likelihood.MarkovState()
info = borg.likelihood.LikelihoodInfo()
info["GRID_LENGTH"] = np.array([-L//2, L//2, -L//2, L//2, -L//2, L//2])
info["GRID"] = np.array([N, N, N], dtype=np.uint32)
box = borg.forward.BoxModel(L, N)

# Set up an example Markov State
# We only add the necessary parts here
chain = build_gravity_model(state,box)
state.newForwardModel("BORG_model",chain)
state.newScalar("cosmology",cpar)
state.newScalar("corner0",-L/2)
state.newScalar("corner1",-L/2)
state.newScalar("corner2",-L/2)

# Construct the likelihood object
like = build_likelihood(state, info)

# Run the basic likelihood functions as defined in Likelihood.py
like.initializeLikelihood(state)

like.updateMetaParameters(state)

# Generate a GRF field as the initial conditions from which we run the forward model
s_hat = np.fft.rfftn(np.random.randn(N, N, N) / N**(1.5))

like.generateMockData(s_hat, state)
like.logLikelihood(s_hat)

# Get the analytic adjoint gradient
analytic_gradient = like.gradientLikelihood(s_hat)

# Get the numerical gradient by using finite differencing on the loglikelihood.
num_gradient = np.zeros((N, N, N // 2 + 1), dtype=np.complex128)
s_hat_epsilon = s_hat.copy()
# May have to play around with epsilon in order to get good gradient 
epsilon = 0.001
for i in tqdm(range(N)):
    for j in range(N):
        for k in range(N//2+1):
            s_hat_epsilon[i, j, k] = s_hat[i, j, k] + epsilon
            L = like.logLikelihood(s_hat_epsilon)
            s_hat_epsilon[i, j, k] = s_hat[i, j, k] - epsilon
            L -= like.logLikelihood(s_hat_epsilon)
            QQ = L / (2.0 * epsilon)

            s_hat_epsilon[i, j, k] = s_hat[i, j, k] + 1j * epsilon
            L = like.logLikelihood(s_hat_epsilon)
            s_hat_epsilon[i, j, k] = s_hat[i, j, k] - 1j * epsilon
            L -= like.logLikelihood(s_hat_epsilon)
            QQ = QQ + L * 1j / (2.0 * epsilon)

            s_hat_epsilon[i, j, k] = s_hat[i, j, k]

            num_gradient[i, j, k] = QQ

np.savez("gradients.npz", num=num_gradient, ana=analytic_gradient)
