# Defines the VIRBIUS_BORG_Likelihood class for use with PyBORG

import borg
import numpy as np
import cython
from get_linear_v_field import get_linear_v_field
from v_field_interpolation import interpolate_linear_v_field_vc4
from tools import tangent_adjoint_1_vc2, tangent_adjoint_2

cons = borg.console()

myprint = lambda x: cons.print_std(x) if type(x) == str else cons.print_std(
    repr(x))

class VIRBIUS_BORG_Likelihood(borg.likelihood.BaseLikelihood):
    def __init__(self, fwd, N, L):
        myprint(f" Init {N}, {L}")
        super().__init__(fwd, N, L)

    def initializeLikelihood(self, state):
        myprint("Init likelihood")
        fwd = self.getForwardModel()
        
        # TEST to get hades_python to work:
        state.newArray1d('data_x',1000, True)
        state.newArray1d("data_y", 1000, True)
        state.newArray1d("data_z", 1000, True)
        state.newArray1d("tracer_velocities", 1000, True)
        self.data = [state['data_x'],state['data_y'],state['data_z'],state['tracer_velocities']]
        state.newArray1d("interpolated_velocities",  self.data[0].shape[0], True)
        state.newArray3d("final_density_field", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_0", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_1", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_2", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        


    def updateMetaParameters(self, state):
        cpar = state['cosmology']
        myprint(f"Cosmology is {cpar}")
        self.getForwardModel().setCosmoParams(cpar)

    def generateMockData(self, s_hat, state):
        fwd = self.getForwardModel()
        
        # Generate the mock data.
        state['data_x'][:] = np.random.uniform(-fwd.getBoxModel().L[0]/2, fwd.getBoxModel().L[0]/2, size=(self.data[0].shape[0]))
        state['data_y'][:] = np.random.uniform(-fwd.getBoxModel().L[0]/2, fwd.getBoxModel().L[0]/2, size=(self.data[0].shape[0]))
        state['data_z'][:] = np.random.uniform(-fwd.getBoxModel().L[0]/2, fwd.getBoxModel().L[0]/2, size=(self.data[0].shape[0]))
               
        # Run the necessary parts of the chain
        output_density = np.zeros(fwd.getOutputBoxModel().N)
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)
        state['final_density_field'][:] = output_density
        cpar = state['cosmology']
        output_velocity = np.zeros((3,fwd.getBoxModel().N[0],fwd.getBoxModel().N[0],fwd.getBoxModel().N[0]))     
        output_velocity = get_linear_v_field(output_density,fwd.getOutputBoxModel().N[0],fwd.getOutputBoxModel().L[0],cpar.h*100,cpar.omega_m**(5/9))
        state['linear_velocity_field_0'][:] = output_velocity[0,:,:,:]
        state['linear_velocity_field_1'][:] = output_velocity[1,:,:,:]
        state['linear_velocity_field_2'][:] = output_velocity[2,:,:,:]
        tracer_number =  state['data_x'].shape[0]
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity, kernel_sum = interpolate_linear_v_field_vc4(output_velocity,self.data[0],self.data[1], self.data[2], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().L[0], fwd.getOutputBoxModel().xmin[0])
        state['interpolated_velocities'][:] = interpolated_velocity
        state['tracer_velocities'][:] = np.random.uniform(-np.amax(interpolated_velocity),np.amax(interpolated_velocity), size=(self.data[0].shape[0]))

        # Produce the likelihood
        sigma_noise = 1
        like = ((state['tracer_velocities'][:] - interpolated_velocity)**2).sum() / (2*sigma_noise**2)
        myprint(
            f"Initial log_likelihood: {like}, var(s_hat) = {np.var(s_hat)}")

    def logLikelihoodComplex(self, s_hat, gradientIsNext):
    #def logLikelihoodComplex(self, s_hat):
        fwd = self.getForwardModel()
        cpar = borg.cosmo.CosmologicalParameters() 
        # Run the necessary parts of the chain
        output_density = np.zeros(fwd.getOutputBoxModel().N)
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)
        output_velocity = np.zeros((3,fwd.getBoxModel().N[0],fwd.getBoxModel().N[0],fwd.getBoxModel().N[0]))     
        output_velocity = get_linear_v_field(output_density,fwd.getOutputBoxModel().N[0],fwd.getOutputBoxModel().L[0],cpar.h*100,cpar.omega_m**(5/9))
        tracer_number =  self.data[0].shape[0]
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity, kernel_sum = interpolate_linear_v_field_vc4(output_velocity,self.data[0],self.data[1], self.data[2], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().L[0], fwd.getOutputBoxModel().xmin[0])
        
        # Piecewise test 1: the gravity model function deltaI -> deltaF
        L = -0.5 * (output_density**2).sum()
        
        # Piecewise test 2: we add the section deltaF -> v_grid
        # We build a Gaussian likelihood for this part too.
        #L = -0.5 *  (output_velocity**2).sum()
        
        #Piecewise test 3: we add the section vgrid -> v_tracer. Your interpolation does the interpolation and projection in one, so you have 
        # both factors here in one.
        #L = -0.5 * (interpolated_velocity**2).sum()
        
        
        # Calculate the likelihood
        sigma_noise = 1
        #L = 0.5 * ((self.data[3] - interpolated_velocity)**2).sum() / (sigma_noise**2)

        myprint(f"var(s_hat): {np.var(s_hat)}, Call to logLike: {L}")
        return L

    def gradientLikelihoodComplex(self, s_hat):
        fwd = self.getForwardModel()
        cpar = borg.cosmo.CosmologicalParameters() 
        # Run the necessary parts of the chain
        output_density = np.zeros(fwd.getOutputBoxModel().N)
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)
        output_velocity = np.zeros((3,fwd.getBoxModel().N[0],fwd.getBoxModel().N[0],fwd.getBoxModel().N[0]))     
        output_velocity = get_linear_v_field(output_density,fwd.getOutputBoxModel().N[0],fwd.getOutputBoxModel().L[0],cpar.h*100,cpar.omega_m**(5/9))
        tracer_number =  self.data[0].shape[0]
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity, kernel_sum = interpolate_linear_v_field_vc4(output_velocity,self.data[0],self.data[1], self.data[2], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().L[0], fwd.getOutputBoxModel().xmin[0])
        
        # We build the BORG grid and fourier modes
        L = fwd.getBoxModel().L[0]
        N = fwd.getBoxModel().N[0]
        corner0 = fwd.getBoxModel().xmin[0]
        corner1 = fwd.getBoxModel().xmin[1]
        corner2 = fwd.getBoxModel().xmin[2]
        # 06/01/21 Next two lines commented out to test a fix for voxel centres
        #delta = L/(N-1)
        #grid_axis = np.linspace(-L/2,L/2,N)
        delta = L/N
        grid_axis_0 = np.linspace(corner0+delta/2,corner0+L-delta/2,N)
        grid_axis_1 = np.linspace(corner1+delta/2,corner1+L-delta/2,N)
        grid_axis_2 = np.linspace(corner2+delta/2,corner2+L-delta/2,N)
        grid_y, grid_x, grid_z = np.meshgrid(grid_axis_0,grid_axis_1,grid_axis_2)
        ik = np.fft.fftfreq(N,d=delta)*2*np.pi
        k_1,k_0,k_2 = np.meshgrid(ik,ik,ik)
        k_sq = np.multiply(k_0,k_0) + np.multiply(k_1,k_1) + np.multiply(k_2,k_2)
        k_sq[k_sq==0] = 0.00000001
                
        # We require the Finite Difference Approximation kernels here.
        # The kernel for the gradient:
        D_0 = -1j * cpar.h*100 * cpar.omega_m**(5/9) * np.sin(k_0) / k_sq
        D_1 = -1j * cpar.h*100 * cpar.omega_m**(5/9) * np.sin(k_1) / k_sq
        D_2 = -1j * cpar.h*100 * cpar.omega_m**(5/9) * np.sin(k_2) / k_sq
        
        sigma_noise = 200
        #v_tracer_tilde = -(self.data[3] - interpolated_velocity)/sigma_noise**2
        #v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2 = tangent_adjoint_1_vc2(self.data[0], self.data[1], self.data[2], N, L, corner0, grid_x, grid_y, grid_z, delta, v_tracer_tilde, kernel_sum)       
        #delta_F_tilde =  tangent_adjoint_2(N, D_0, D_1, D_2, v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2)
        
        #piecewise test 1: we feed only a deltaF_tilde function. This uses the complex half representation.
        delta_F_tilde_test = np.zeros((N,N,N))
        #dmin = np.amin(output_density)
        #dmax = np.amax(output_density)
        delta_F_tilde_test = -1/2* output_density
        
        #piecewise test 2: we need to add the velocity field on the grid part.
        #v_grid_tilde_0 = output_velocity[0]
        #v_grid_tilde_1 = output_velocity[1]
        #v_grid_tilde_2 = output_velocity[2]
        #delta_F_tilde_test = tangent_adjoint_2(N, D_0, D_1, D_2, v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2)

        #piecewise test 3: we add the velocity at tracer positions 
        #v_tracer_tilde = -interpolated_velocity/sigma_noise**2
        #v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2 = tangent_adjoint_1_vc2(self.data[0], self.data[1], self.data[2], N, L, corner0, grid_x, grid_y, grid_z, delta, v_tracer_tilde, kernel_sum)       
        #delta_F_tilde_test =  tangent_adjoint_2(N, D_0, D_1, D_2, v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2)
        
        
        # The output of the above is an array of size (N,N,N) which we denote as delta_F_tilde. This is what we feed to pyborg machinery.
        fwd.adjointModel_v2(delta_F_tilde_test)
        mygrad_hat = np.zeros(s_hat.shape, dtype=np.complex128)
        fwd.getAdjointModel(mygrad_hat)
        return  mygrad_hat
        
model = None  
  
@borg.registerGravityBuilder
def build_gravity_model(state, box):
    global model
    chain = borg.forward.ChainForwardModel(box)
    chain.addModel(borg.forward.models.HermiticEnforcer(box))
    chain.addModel(borg.forward.models.Primordial(box, 0.001))
    chain.addModel(borg.forward.models.EisensteinHu(box))
    lpt = borg.forward.models.BorgLpt(box=box,box_out=box,ai=0.001,af=1.0,supersampling=1)
    chain.addModel(lpt)
    model = chain
    return chain


@borg.registerLikelihoodBuilder
def build_likelihood(state, info):
    boxm = model.getBoxModel()
    return VIRBIUS_BORG_Likelihood(model, boxm.N, boxm.L)

@borg.registerSamplerBuilder
def build_sampler(state, info):
    #sampler = borg.samplers.HMCDensitySampler(VIRBIUS_BORG_Likelihood)
    return []  