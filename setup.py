from setuptools import setup
from Cython.Build import cythonize

setup(
    name="Linear_velocity_pyborg",
    ext_modules=cythonize("*.pyx",annotate=True),
)

