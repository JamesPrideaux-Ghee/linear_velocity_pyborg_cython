# A cython-enabled version of the VIRBIUS-BORG Likelihood

import borg
import numpy as np
import cython
from get_linear_v_field import get_linear_v_field
from v_field_interpolation import interpolate_linear_v_field, interpolated_field_projection
from adjoint_gradient import tangent_adjoint_1, tangent_adjoint_2, tangent_adjoint_3

cons = borg.console()
myprint = lambda x: cons.print_std(x) if type(x) == str else cons.print_std(repr(x))

class VIRBIUS_BORG_Likelihood(borg.likelihood.BaseLikelihood):
    def __init__(self, fwd, N, L):
        myprint(f" Init {N}, {L}")
        super().__init__(fwd, N, L)


    def initializeLikelihood(self, state):
        myprint("Init likelihood")
        fwd = self.getForwardModel()

        state.newArray1d('data_x',4096, True)
        state.newArray1d("data_y", 4096, True)
        state.newArray1d("data_z", 4096, True)
        state.newArray1d("tracer_velocities", 4096, True)
        self.data = [state['data_x'],state['data_y'],state['data_z'],state['tracer_velocities']]
        state.newArray1d("interpolated_velocities",  self.data[0].shape[0], True)
        state.newArray3d("final_density_field", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_0", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_1", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
        state.newArray3d("linear_velocity_field_2", fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], fwd.getOutputBoxModel().N[0], True)
      

    def updateMetaParameters(self, state):
        cpar = state['cosmology']
        myprint(f"Cosmology is {cpar}")
        self.getForwardModel().setCosmoParams(cpar)

    def generateMockData(self, s_hat, state):
        # Extract the BORG box parameters
        fwd = self.getForwardModel()
        N = fwd.getBoxModel().N[0]
        L = fwd.getOutputBoxModel().L[0]
        corner0 = fwd.getOutputBoxModel().xmin[0]
        corner1 = fwd.getOutputBoxModel().xmin[1]
        corner2 = fwd.getOutputBoxModel().xmin[2]
        
        # Extract data from mock catalogue
        fdir = ''
        file = 'catalogue.npz'
        npzfile = np.load(fdir+file)
        data_x = npzfile['data_x']
        data_y = npzfile['data_y']
        data_z = npzfile['data_z']
        tracer_velocities = npzfile['tracer_velocities']
        
        state['data_x'][:] = data_x
        state['data_y'][:] = data_y
        state['data_z'][:] = data_z
        state['tracer_velocities'][:] = tracer_velocities

        # Run the necessary parts of the chain
        output_density = np.zeros((N,N,N))
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)
        cpar = state['cosmology']
        
        # Get the linear velocity field from the density field
        output_velocity = np.zeros((3,N,N,N))     
        output_velocity = get_linear_v_field(output_density,N,L,cpar.h*100,cpar.omega_m**(5/9))
        tracer_number =  state['data_x'].shape[0]
        
        # Interpolate the linear velocity field to the tracer positions and get the line of sight component
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2 = interpolate_linear_v_field(output_velocity,self.data[0],self.data[1], self.data[2], N, L, corner0,corner1,corner2)
        interpolated_velocity = interpolated_field_projection(self.data[0],self.data[1], self.data[2],interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2)

        sigma_noise = 200
        like = ((state['tracer_velocities'][:] - interpolated_velocity)**2).sum() / (2*sigma_noise**2)
        myprint(
            f"Initial log_likelihood: {like}, var(s_hat) = {np.var(s_hat)}")


    def logLikelihoodComplex(self, s_hat, gradientIsNext):
    #def logLikelihoodComplex(self, s_hat):
        fwd = self.getForwardModel()
        N = fwd.getBoxModel().N[0]
        L = fwd.getOutputBoxModel().L[0]
        corner0 = fwd.getOutputBoxModel().xmin[0]
        corner1 = fwd.getOutputBoxModel().xmin[1]
        corner2 = fwd.getOutputBoxModel().xmin[2]
        
        cpar = borg.cosmo.CosmologicalParameters() 
        # Run the necessary parts of the chain
        output_density = np.zeros((N,N,N))
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)

        # Get the linear velocity field from the density field
        output_velocity = np.zeros((3,N,N,N))     
        output_velocity = get_linear_v_field(output_density,N,L,cpar.h*100,cpar.omega_m**(5/9))
        tracer_number =  self.data[0].shape[0]
        
        # Interpolate the linear velocity field to the tracer positions and get the line of sight component
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2 = interpolate_linear_v_field(output_velocity,self.data[0],self.data[1], self.data[2], N, L, corner0,corner1,corner2)
        interpolated_velocity = interpolated_field_projection(self.data[0],self.data[1], self.data[2],interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2)
        
        # Calculate the likelihood
        sigma_noise = 200
        L = 0.5 * ((self.data[3] - interpolated_velocity)**2).sum() / (sigma_noise**2)

        myprint(f"var(s_hat): {np.var(s_hat)}, Call to logLike: {L}")
        return L


    def gradientLikelihoodComplex(self, s_hat):
        fwd = self.getForwardModel()
        N = fwd.getBoxModel().N[0]
        L = fwd.getOutputBoxModel().L[0]
        corner0 = fwd.getOutputBoxModel().xmin[0]
        corner1 = fwd.getOutputBoxModel().xmin[1]
        corner2 = fwd.getOutputBoxModel().xmin[2]
        cpar = borg.cosmo.CosmologicalParameters() 
        
        # Run the necessary parts of the chain
        output_density = np.zeros((N,N,N))
        fwd.forwardModel_v2(s_hat)
        fwd.getDensityFinal(output_density)

        # Get the linear velocity field from the density field
        output_velocity = np.zeros((3,N,N,N))     
        output_velocity = get_linear_v_field(output_density,N,L,cpar.h*100,cpar.omega_m**(5/9))
        tracer_number =  self.data[0].shape[0]
        
        # Interpolate the linear velocity field to the tracer positions and get the line of sight component
        interpolated_velocity = np.zeros((tracer_number))
        interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2 = interpolate_linear_v_field(output_velocity,self.data[0],self.data[1], self.data[2], N, L, corner0,corner1,corner2)
        interpolated_velocity = interpolated_field_projection(self.data[0],self.data[1], self.data[2],interpolated_velocity_0, interpolated_velocity_1,interpolated_velocity_2)
        
        # Build the Fourier modes of the box
        delta = L/N
        ik = np.fft.fftfreq(N,d=delta)*2*np.pi
        k_1,k_0,k_2 = np.meshgrid(ik,ik,ik)
        k_sq = np.multiply(k_0,k_0) + np.multiply(k_1,k_1) + np.multiply(k_2,k_2)
        k_sq[k_sq==0] = 0.00000001
                
        # We require the Finite Difference Approximation kernels here.
        # The kernel for the gradient:
        D_0 = -1j * cpar.h*100 * cpar.omega_m**(5/9) * np.sin(k_0) / k_sq
        D_1 = -1j * cpar.h*100 * cpar.omega_m**(5/9) * np.sin(k_1) / k_sq
        D_2 = -1j * cpar.h*100 * cpar.omega_m**(5/9) * np.sin(k_2) / k_sq
        
        sigma_noise = 200
        v_tracer_tilde = (self.data[3] - interpolated_velocity)/sigma_noise**2
        v_tracer_tilde_0,v_tracer_tilde_1,v_tracer_tilde_2 = tangent_adjoint_1(self.data[0], self.data[1], self.data[2],v_tracer_tilde)
        v_grid_tilde_0,v_grid_tilde_1,v_grid_tilde_2 = tangent_adjoint_2(self.data[0], self.data[1], self.data[2],N,L,corner0,corner1,corner2,v_tracer_tilde_0,v_tracer_tilde_1,v_tracer_tilde_2)
        delta_F_tilde = tangent_adjoint_3(N, D_0, D_1, D_2, v_grid_tilde_0, v_grid_tilde_1, v_grid_tilde_2)
        
        # The output of the above is an array of size (N,N,N) which we denote as delta_F_tilde. This is what we feed to pyborg machinery.
        fwd.adjointModel_v2(delta_F_tilde)
        mygrad_hat = np.zeros(s_hat.shape, dtype=np.complex128)
        fwd.getAdjointModel(mygrad_hat)
        # Enforce Nyquist plane condition
        mygrad_hat[N//2,:,:] = 0
        mygrad_hat[:,N//2,:] = 0
        mygrad_hat[:,:,N//2] = 0
        
        return -1/2 * mygrad_hat


model = None  
  
@borg.registerGravityBuilder
def build_gravity_model(state, box):
    global model
    chain = borg.forward.ChainForwardModel(box)
    chain.addModel(borg.forward.models.HermiticEnforcer(box))
    chain.addModel(borg.forward.models.Primordial(box, 0.001))
    chain.addModel(borg.forward.models.EisensteinHu(box))
    lpt = borg.forward.models.BorgLpt(box=box,box_out=box,ai=0.001,af=1.0,supersampling=2)
    chain.addModel(lpt)
    model = chain
    return chain


@borg.registerLikelihoodBuilder
def build_likelihood(state, info):
    boxm = model.getBoxModel()
    return VIRBIUS_BORG_Likelihood(model, boxm.N, boxm.L)

@borg.registerSamplerBuilder
def build_sampler(state, info):
    #sampler = borg.samplers.HMCDensitySampler(VIRBIUS_BORG_Likelihood)
    return []  
