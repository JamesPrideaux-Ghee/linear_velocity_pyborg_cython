This directory contains the cython codes to run BORG with a Gaussian likelihood for the peculiar velocity field.

Python prerequisites: 
- python3.6 or greater
- numpy
- cython

Code description:
- The aim of the code within this repository is to allow one to run BORG using simplified peculiar velocity data as the constraints for the initial conditions. 
- In it's 'standard' form, BORG constrains the initial conditions using galaxy clustering data. This uses a) a likelihood defined at voxel positions for galaxy number counts, and b) a model for galaxy bias to compare data and predictions. 
- Instead, here, we aim to use a) a likelihood defined at tracer positions, and b) the assumption of no velocity bias (as well as no Malmquist bias)
- To simplify, we assume the distance to tracers is known exactly, so that the data used in BORG is an exact comoving position, a peculiar velocity, and its associated error. We call such data 'peculiar velocity tracers' to distinguish them from the true distance tracers (i.e. where we have uncertain distances)
- This code is built to run within the pyborg framework. The likelihood is written within the python wrapper framework BORG needs, and fed to the standard C++ machinery. Here, once BORG is compiled with pyborg and hades_python enabled (allowing sampling using a python likelihood), BORG only needs to be passed the Likelihood.py file. Additional functions that define the data model and adjoint gradient (written in python) are evaluated in this file.

Data model:
- We use a simplified almost linear forward model here:
  a) We evolve the initial conditions to z=0 using LPT
  b) We get the linear velocity field from the final density using the linearised continuity equation (nabla . v = -H0 * f delta_f)
  c) We interpolate this velocity field to tracer positions, and take the line-of-sight component
  d) We compare this line-of-sight component to the data


File descriptions:
- adjoint_gradient.pyx: contains three functions (tangent_adjoint_projection,tangent_adjoint_interpolation,tangent_adjoint_get_linear_v_field) that are run 
in the given order to calculate the adjoint gradient of this forward model. 
- get_linear_v_field.pyx: contains a single function (get_linear_v_field) which takes as an argument a BORG density 
contrast field, and produces the associated linear velocity field, as calculated using Eulerian Perturbation theory.
- FM_Likelihood.py: this is the python file which is required by the BORG ini file. The form given here is an example, which
loads data from an external catalogue.npz file. The functions within the other files of this repository are set up with
their correct arguments and orders, as a demonstration of how to use them. This should work out of the box.
  - WF_Likelihood.py runs similarly to FM_Likelihood.py, but evolves the initial conditions linearly
  - FM_mockdata_Likelihood.py generates mock data on the fly, rather than loading externally generated data
  - FM_halo_Likelihood.py loads Rockstar halos as tracers (assumes the outputs are in the default Rockstar order)
- setup.py: this is the cython setup file, which is run to compile the rest of the cython code. Moving this to the same directory
as the three .pyx files, one should run 'python setup.py build', which compiles the three .pyx files in a build directory. One
can add an extra argument to choose a specific compiler, for example 'CC = gcc python setup.py build'. NOTE: one should compile
the cython .pyx files using the same python version that is being used to run the hades_python executable.
- v_field_interpolation.pyx: contains two functions (interpolate_linear_v_field, project_interpolated_field) that perform the CiC
interpolation of a linear velocity field to the positions of a set of tracers, and then take the line of sight component of the 
velocity field at tracer positions respectively. 
